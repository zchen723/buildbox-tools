# buildbox-tools

Command-line utilities for BuildBox, BuildGrid, and other Remote Execution
services.

Please refer to each individual tool for usage and build instructions.

## casdownload

`casdownload` facilitates downloading blobs, action outputs and directory trees
from a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

Please refer to the [README.md](cpp/casdownload/README.md) for more information.

## casupload

`casupload` facilitates uploading blobs, action outputs and directory trees
to a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

Please refer to the [README.md](cpp/casupload/README.md) for more information.

## chrootbuilder

`chrootbuilder` generates chroot images (filesystem tarballs)
from a docker container or image. It can also make the chroot image compatible with
`buildbox-run-userchroot`.

Please refer to the [README.md](python/chrootbuilder/README.md) for more information.

## LogStream tools

These small utilities can be used to send and receive logs using the [LogStream API](https://groups.google.com/forum/#!msg/remote-execution-apis/LCLsBSgSnU0/yTTtqr-cAwAJ).

### logstreamreceiver
Listens for LogStream API `ByteStream.Write()` requests and echoes the data it receives to `stdout`.

### logstreamtail
Issues LogStream API `ByteStream.Read()` requests and prints the data it reads to `stdout`.

### outputstreamer
Reads from `stdin` and streams it to the given ByteStream endpoint.

## Building

The tools under `cpp/` depend on [`buildbox-common`](https://gitlab.com/BuildGrid/buildbox/buildbox-common/)
which is buildbox's core SDK containing tooling for CAS, Execution Service and Action Cache interaction. (Please refer to the `buildbox-common` [README](https://gitlab.com/BuildGrid/buildbox/buildbox-common/) for guidance on building it from source.)


In order to build them:

```
# From the root of `buildbox-tools`:
$ mkdir build
$ cd build
$ cmake ../cpp -DBuildboxCommon_DIR=/path/to/buildbox-common-installation/usr/local/lib/cmake/BuildboxCommon/
$ make
```
