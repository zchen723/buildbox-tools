#casupload

## Usage

```
Usage: ./casupload --cas-server=ADDRESSS [--instance=INSTANCE]
                    [--access-token=TOKEN_PATH]
                   [--follow-symlinks | -f] [--dry-run | -d]
                   [--output-digest-file=<FILE>] <paths>

Uploads the given files and directories to CAS, then prints the digest
hash and size of the corresponding Directory messages.

The files are placed in CAS subdirectories corresponding to their
"paths. For example, 'casupload file1.txt subdir/file2.txt' would create
a CAS directory containing file1.txt and a subdirectory called 'subdir'
containing file2.txt.

The directories will be uploaded individually as merkle trees.
The merkle tree for a directory will contain all of the content
within the directory.

The server and instance to write to are controlled by the ADDRESS
and INSTANCE arguments.

By default 'casupload' will not follow symlinks. Use option -f or
'--links' to alter this behavior.

Access token authentication is optional, and enabled via the
--access-token which specifies the disk path to the token in plaintext

If `--dry-run` is set, digests will be calculated and printed but
no transfers to the remote will take place.

If `--output-digest-file=<FILE>` is set, the output digest will be
written to <FILE> in the form "<HASH>/<SIZE_BYTES>".

Example usage:
casupload --cas-server=http://localhost:50051 foo.c foo.h
```

## Building from Source

`casupload`'s only direct dependency is [`buildbox-common`](https://gitlab.com/BuildGrid/buildbox/buildbox-common/)
which is buildbox's core SDK containing tooling for CAS, Execution Service and
Action Cache interaction.

Please refer to the `buildbox-common` [README](https://gitlab.com/BuildGrid/buildbox/buildbox-common/) for guidance on building it from source.
```
#From the root of `buildbox-tools`:
$ mkdir build
$ cd build
$ cmake ../cpp -DBuildboxCommon_DIR=/path/to/buildbox-common-installation/usr/local/lib/cmake/BuildboxCommon/
$ make
...
$ ./casupload
```
