#ifndef INCLUDED_CASUPLOAD_PROCESSARGS
#define INCLUDED_CASUPLOAD_PROCESSARGS

#include <string>
#include <vector>

namespace casupload {
struct ProcessedArgs {
    std::string output_digest_file = "";
    std::vector<std::string> paths = std::vector<std::string>();
    std::string casServerAddress = "", instance = "", tokenPath = "";
    bool followSymlinks = false;
    bool dryRunMode = false;
    bool help = false;
};

struct ProcessedArgs processArgs(int argc, char *argv[]);
} // namespace casupload

#endif
