#include <processargs.h>

#include <cstdlib>
#include <cstring>

casupload::ProcessedArgs casupload::processArgs(int argc, char *argv[])
{
    casupload::ProcessedArgs processedArgs;

    for (auto i = 1; i < argc; i++) {
        const std::string argument_value(argv[i]);
        if (argument_value == "--help" || argument_value == "-h") {
            processedArgs.help = true;
            return processedArgs;
        }

        if (argument_value.find("--instance=") == 0) {
            processedArgs.instance =
                argument_value.substr(strlen("--instance="));
        }
        else if (argument_value.find("--access-token=") == 0) {
            processedArgs.tokenPath =
                argument_value.substr(strlen("--access-token="));
        }
        else if (argument_value.find("--cas-server=") == 0) {
            processedArgs.casServerAddress =
                argument_value.substr(strlen("--cas-server="));
        }
        else if (argument_value == "--follow-symlinks" ||
                 argument_value == "-f") {
            processedArgs.followSymlinks = true;
        }
        else if (argument_value == "--dry-run" || argument_value == "-d") {
            processedArgs.dryRunMode = true;
        }
        else if (argument_value.rfind("--output-digest-file=", 0) == 0) {
            std::string arg_prefix = "--output-digest-file=";
            processedArgs.output_digest_file =
                argument_value.substr(arg_prefix.length());
        }
        else {
            processedArgs.paths.push_back(argument_value);
        }
    }
    return processedArgs;
}
