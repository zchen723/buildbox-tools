#include <gtest/gtest.h>
#include <iostream>

#include <processargs.h>

TEST(CasuploadArgs, HTest)
{
    const char *argv[2] = {"casupload", "-h"};
    ASSERT_TRUE(casupload::processArgs(2, const_cast<char **>(argv)).help);
}

TEST(CasuploadArgs, HelpTest)
{
    const char *argv[2] = {"casupload", "--help"};
    ASSERT_TRUE(casupload::processArgs(2, const_cast<char **>(argv)).help);
}

TEST(CasuploadArgs, InstanceTest)
{
    const char *argv[2] = {"casupload", "--instance=INSTANCE"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).instance ==
        "INSTANCE");
}

TEST(CasuploadArgs, TokenTest)
{
    const char *argv[2] = {"casupload", "--access-token=TOKEN"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).tokenPath ==
        "TOKEN");
}

TEST(CasuploadArgs, CasServerTest)
{
    const char *argv[2] = {"casupload", "--cas-server=SERVER"};
    ASSERT_TRUE(casupload::processArgs(2, const_cast<char **>(argv))
                    .casServerAddress == "SERVER");
}

TEST(CasuploadArgs, FTest)
{
    const char *argv[2] = {"casupload", "-f"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).followSymlinks ==
        true);
}

TEST(CasuploadArgs, FollowSymlinksTest)
{
    const char *argv[2] = {"casupload", "--follow-symlinks"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).followSymlinks ==
        true);
}

TEST(CasuploadArgs, DTest)
{
    const char *argv[2] = {"casupload", "-d"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).dryRunMode ==
        true);
}

TEST(CasuploadArgs, DryRunModeTest)
{
    const char *argv[2] = {"casupload", "--dry-run"};
    ASSERT_TRUE(
        casupload::processArgs(2, const_cast<char **>(argv)).dryRunMode ==
        true);
}

TEST(CasuploadArgs, OutputDigestTest)
{
    const char *argv[2] = {"casupload", "--output-digest-file=FILE"};
    ASSERT_TRUE(casupload::processArgs(2, const_cast<char **>(argv))
                    .output_digest_file == "FILE");
}

TEST(CasuploadArgs, MultipleArgsTest)
{
    const char *argv[4] = {"casupload", "--instance=INSTANCE",
                           "--access-token=TOKEN",
                           "--output-digest-file=FILE"};
    casupload::ProcessedArgs processedArgs =
        casupload::processArgs(4, const_cast<char **>(argv));
    ASSERT_TRUE(processedArgs.followSymlinks == false);
    ASSERT_TRUE(processedArgs.dryRunMode == false);
    ASSERT_TRUE(processedArgs.tokenPath == "TOKEN");
    ASSERT_TRUE(processedArgs.instance == "INSTANCE");
    ASSERT_TRUE(processedArgs.output_digest_file == "FILE");
}
