# casdownload

## Usage

```
Usage: ./casdownload
   --help                Display usage and exit
   --instance            CAS server instance name [required]
   --cas-server          CAS server end-point/url [required]
   --destination-dir     Directory to save downloaded data [required]
   --root-digest         Download a tree by its root node ID [optional]
   --action-digest       Download the stderr, stdout, output files and directories of this digest via the ActionCache [optional]
   --file-digest         Download a blob by its content [optional]

Description:
  Download the specified digest(root/action/file) into the specified `--destination-dir` directory from the `--instance` of the CAS configured at `--cas-server`
  Digest can be one of:
    --root-digest   - Download a tree by its root node ID
    --action-digest - Download the stderr, stdout, output files and directories of the specified action via the ActionCache. The ActionCache must be hosted in the same server as the CAS server at `--cas-server`
    --file-digest   - Download a blob by its content to `--destination-dir`/blob

Example usage:
  casdownload --instance=dev --cas-server=http://localhost:50051 --root-digest=deadbeef/412 --destination-dir=/path/to/dir
```

### Examples with Docker

`casdownload` can be used via the [`Dockerfile`](Dockerfile). The following
illustrates the `file-digest` mode to download a single blob from the CAS:

```
$ cd cpp/casdownload
$ docker build . -t casdownload
...
$ docker run -it casdownload bash
$ casdownload --instance=dev --cas-server=http://my-cas-server:60051 --file-digest=9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 --destination-dir=dir
CAS client connecting to http://my-cas-server:60051
Starting to download 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir"
Downloaded blob to dir/blob
Finished downloading 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir" in 0.036 second(s)
$ cat dir/blob
Hello, world!
$
```

To download an Action:

```
$ casdownload --instance=dev --cas-server=http://my-cas-server:40051 --action-digest=0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 --destination-dir=actionresult
CAS client connecting to http://my-cas-server:40051
Starting to download 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult"
Finished downloading 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult" in 1.485 second(s)
$ cat actionresult/stdout
Hello, world stdout!
$ cat actionresult/stderr
Hello, world stderr!
$ ls actionresult
myoutputfile
stderr
stdout
$
```

## Building from Source

`casdownload`'s only direct dependency is [`buildbox-common`](https://gitlab.com/BuildGrid/buildbox/buildbox-common/)
which is buildbox's core SDK containing tooling for CAS, Execution Service and
Action Cache interaction.

Please refer to the `buildbox-common` [README](https://gitlab.com/BuildGrid/buildbox/buildbox-common/) for guidance on building it from source.
```
# From the root of `buildbox-tools`:
$ mkdir build
$ cd build
$ cmake ../cpp -DBuildboxCommon_DIR=/path/to/buildbox-common-installation/usr/local/lib/cmake/BuildboxCommon/
$ make
...
$ ./casdownload
```
