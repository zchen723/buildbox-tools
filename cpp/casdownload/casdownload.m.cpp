/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_client.h>
#include <buildboxcommon_commandline.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protojsonutils.h>
#include <buildboxcommon_protos.h>

#include <chrono>
#include <fcntl.h>
#include <iomanip>
#include <iostream>
#include <libgen.h> // dirname
#include <memory>
#include <regex>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using buildboxcommon::Digest;

bool actionResultFromAction(const Digest &action,
                            const buildboxcommon::ConnectionOptions &options,
                            buildboxcommon::Client *client,
                            buildboxcommon::ActionResult *actionResult)
{
    std::unique_ptr<buildboxcommon::ActionCache::Stub> actionCache =
        buildboxcommon::ActionCache::NewStub(options.createChannel());

    buildboxcommon::GetActionResultRequest actionRequest;
    actionRequest.set_instance_name(client->instanceName());

    actionRequest.set_inline_stdout(false);
    actionRequest.set_inline_stderr(false);
    *actionRequest.mutable_action_digest() = action;

    grpc::ClientContext context;
    const grpc::Status status =
        actionCache->GetActionResult(&context, actionRequest, actionResult);

    if (!status.ok()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Action cache returned error "
                                    << std::to_string(status.error_code())
                                    << ": \""
                                    << status.error_message() + "\"");
    }

    return true;
}

std::string calculateDirname(const std::string &path)
{
    // some dirname implementations modify the input so we have to copy
    std::vector<char> cpath(path.cbegin(), path.cend());
    cpath.push_back('\0');
    return std::string(dirname(&cpath[0]));
}
void createDirectoryForPath(const std::string &path)
{
    const std::string dir = calculateDirname(path);
    buildboxcommon::FileUtils::createDirectory(dir.c_str());
}

void writeExitCode(const std::string &file, int code)
{
    std::ostringstream rc;
    rc << code << "\n";
    buildboxcommon::FileUtils::writeFileAtomically(file, rc.str());
    std::cout << "Wrote rc=" << code << " to " << file << std::endl;
}

void writeProtoAsJson(const google::protobuf::Message &message,
                      const std::string &path)
{
    buildboxcommon::ProtoJsonUtils::JsonPrintOptions jsonOptions;
    jsonOptions.add_whitespace = true; // make the output human-readable

    const std::unique_ptr<std::string> json =
        buildboxcommon::ProtoJsonUtils::protoToJsonString(message,
                                                          jsonOptions);

    if (json) {
        buildboxcommon::FileUtils::writeFileAtomically(path, *json);
    }
    else {
        std::cout << "Error converting message to JSON." << std::endl;
    }
}

void fetchAndWriteExecutionStatistics(
    const buildboxcommon::ActionResult &actionResult,
    const std::string &executionStatsDest,
    buildboxcommon::Client *remoteCasClient)
{
    if (actionResult.execution_metadata().auxiliary_metadata().empty()) {
        return;
    }

    Digest d;
    if (!actionResult.execution_metadata().auxiliary_metadata(0).UnpackTo(
            &d)) {
        return;
    }

    BUILDBOX_LOG_DEBUG("ActionResult.execution_metadata.auxiliary_metadata "
                       "contains Digest: ["
                       << d << "], fetching message...");

    google::protobuf::Any anyWrapper;
    try {
        anyWrapper = remoteCasClient->fetchMessage<google::protobuf::Any>(d);
    }
    catch (const std::runtime_error &e) {
        BUILDBOX_LOG_ERROR("Failed to fetch blob referred to by Digest in "
                           "auxiliary metadata ["
                           << d << "]: " << e.what());
        return;
    }

    buildboxcommon::ExecutionStatistics stats;
    if (anyWrapper.UnpackTo(&stats)) {
        BUILDBOX_LOG_DEBUG("Writing `ExecutionStatistics` message to ["
                           << executionStatsDest << "]");
        writeProtoAsJson(stats, executionStatsDest);
    }
    else {
        BUILDBOX_LOG_DEBUG(
            "ActionResult.execution_metadata.auxiliary_metadata did "
            "not point to an `ExecutionStatistics` message, ignoring "
            "it.");
    }
}

void writeActionResultToDisk(const buildboxcommon::ActionResult &actionResult,
                             const std::string &destinationDirectory,
                             const std::string &metadataDir,
                             buildboxcommon::Client *remoteCasClient)

{
    buildboxcommon::Client::OutputMap blobs;
    std::vector<Digest> digests;

    for (const auto &dirIter : actionResult.output_directories()) {
        // Each output_directory is a tree
        const std::string fullPath =
            destinationDirectory + "/" + dirIter.path();
        buildboxcommon::FileUtils::createDirectory(fullPath.c_str());

        const auto tree = remoteCasClient->fetchMessage<buildboxcommon::Tree>(
            dirIter.tree_digest());
        const auto rootDir = tree.root();

        // Directory doesn't have a digest
        // so we iterate through files and directories and download
        for (const auto &file : rootDir.files()) {
            digests.push_back(file.digest());
            blobs.emplace(
                file.digest().hash(),
                buildboxcommon::Client::OutputMap::mapped_type(
                    fullPath + "/" + file.name(), file.is_executable()));
        }

        // DirectoryNode has a digest so we can download directly
        for (const auto &innerDir : rootDir.directories()) {
            remoteCasClient->downloadDirectory(
                innerDir.digest(), fullPath + "/" + innerDir.name());
        }
    }

    for (const auto &fileIter : actionResult.output_files()) {
        digests.push_back(fileIter.digest());
        const std::string fullPath =
            destinationDirectory + "/" + fileIter.path();
        blobs.emplace(fileIter.digest().hash(),
                      buildboxcommon::Client::OutputMap::mapped_type(
                          fullPath, fileIter.is_executable()));

        if (fileIter.path().find("/") != std::string::npos) {
            createDirectoryForPath(fullPath);
        }
    }
    const std::string stdoutDest = metadataDir + "/stdout";
    const std::string stderrDest = metadataDir + "/stderr";
    const std::string exitCodeDest = metadataDir + "/exit_code";
    const std::string metadataDest = metadataDir + "/metadata";
    const std::string executionStatsDest = metadataDir + "/execution_stats";

    if (actionResult.stdout_raw().size()) {
        buildboxcommon::FileUtils::writeFileAtomically(
            stdoutDest, std::string(actionResult.stdout_raw()));
        std::cout << "Wrote inline stdout to: " << metadataDir << std::endl;
    }
    else if (actionResult.stdout_digest() != Digest()) {
        blobs.emplace(
            actionResult.stdout_digest().hash(),
            buildboxcommon::Client::OutputMap::mapped_type(stdoutDest, false));

        digests.push_back(actionResult.stdout_digest());
    }
    if (actionResult.stderr_raw().size()) {
        buildboxcommon::FileUtils::writeFileAtomically(
            stderrDest, actionResult.stderr_raw());
        std::cout << "Wrote inline stderr to: " << stderrDest << std::endl;
    }
    else if (actionResult.stderr_digest() != Digest()) {
        blobs.emplace(
            actionResult.stderr_digest().hash(),
            buildboxcommon::Client::OutputMap::mapped_type(stderrDest, false));

        digests.push_back(actionResult.stderr_digest());
    }

    writeProtoAsJson(actionResult.execution_metadata(), metadataDest);

    fetchAndWriteExecutionStatistics(actionResult, executionStatsDest,
                                     remoteCasClient);

    writeExitCode(exitCodeDest, actionResult.exit_code());
    // TODO: output_symlinks
    remoteCasClient->downloadBlobs(digests, blobs);
}
bool digestFromString(const std::string &s, Digest *digest)
{
    // "[hash in hex notation]/[size_bytes]"
    static const std::regex regex("^([0-9a-fA-F]+)/(\\d+)");
    std::smatch matches;
    if (std::regex_search(s, matches, regex) && matches.size() == 3) {
        const std::string hash = matches[1];
        const std::string size = matches[2];

        digest->set_hash(hash);
        digest->set_size_bytes(std::stoll(size));
        return true;
    }

    return false;
}

int runCasDownload(const buildboxcommon::CommandLine &commandLine)
{
    enum DigestType {
        DIGEST_ROOT,
        DIGEST_ACTION,
        DIGEST_FILE,
        DIGEST_INVALID
    };

    const std::string casServerAddress = commandLine.getString("cas-server");
    const std::string downloadDirectory =
        commandLine.getString("destination-dir");
    const std::string instance = commandLine.getString("instance");

    DigestType digestType = DIGEST_INVALID;
    std::string digest;
    if (commandLine.exists("root-digest")) {
        digestType = DIGEST_ROOT;
        digest = commandLine.getString("root-digest");
    }
    else if (commandLine.exists("action-digest")) {
        digestType = DIGEST_ACTION;
        digest = commandLine.getString("action-digest");
    }
    else if (commandLine.exists("file-digest")) {
        digestType = DIGEST_FILE;
        digest = commandLine.getString("file-digest");
    }

    if (digestType == DIGEST_INVALID) {
        std::cerr << "Error: no valid digest was specified" << std::endl;
        commandLine.usage();
        return 1;
    }

    Digest protoDigest;
    if (!digestFromString(digest, &protoDigest)) {
        std::cerr << "Error: invalid digest \"" << digest << "\"" << std::endl;
        commandLine.usage();
        return 1;
    }

    buildboxcommon::ConnectionOptions connectionOptions;
    connectionOptions.d_url = casServerAddress.c_str();
    connectionOptions.d_instanceName = instance.c_str();

    // connect to remote CAS server
    std::cout << "CAS client connecting to " << casServerAddress << std::endl;

    buildboxcommon::Client remoteCasClient;
    remoteCasClient.init(connectionOptions);

    if (!buildboxcommon::FileUtils::isDirectory(downloadDirectory.c_str())) {
        if (buildboxcommon::FileUtils::isRegularFile(
                downloadDirectory.c_str())) {
            std::cerr
                << "Error: [" << downloadDirectory << "] was specified "
                << "as destination download directory but already exists "
                << "as file.\n"
                << std::endl;
            commandLine.usage();
            return 1;
        }
        const std::string d = calculateDirname(downloadDirectory);
        if (buildboxcommon::FileUtils::isRegularFile(d.c_str())) {
            std::cerr << "Error: [" << downloadDirectory << "] was specified "
                      << "as destination download directory but " << d
                      << " is a file.\n"
                      << std::endl;

            return 1;
        }
    }

    buildboxcommon::FileUtils::createDirectory(downloadDirectory.c_str());
    std::cout << "Starting to download " << protoDigest << " to \""
              << downloadDirectory << "\"" << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    if (digestType == DIGEST_ACTION) {
        buildboxcommon::ActionResult actionResult;
        if (!actionResultFromAction(protoDigest, connectionOptions,
                                    &remoteCasClient, &actionResult)) {
            std::cerr << "Failed to download action result with digest "
                      << digest << std::endl;
            return 1;
        }
        const std::string metadataDir =
            downloadDirectory + "/" + protoDigest.hash() + "-out";
        buildboxcommon::FileUtils::createDirectory(metadataDir.c_str());
        writeActionResultToDisk(actionResult, downloadDirectory, metadataDir,
                                &remoteCasClient);
    }
    else if (digestType == DIGEST_ROOT) {
        remoteCasClient.downloadDirectory(protoDigest, downloadDirectory);
    }
    else {
        try {
            const std::string blob = remoteCasClient.fetchString(protoDigest);
            buildboxcommon::FileUtils::writeFileAtomically(
                downloadDirectory + "/blob", blob);

            std::cout << "Downloaded blob to " << downloadDirectory + "/blob"
                      << std::endl;
        }
        catch (const std::exception &) {
            std::cerr << "Could not download blob with digest " << protoDigest
                      << std::endl;
            return 1;
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;

    std::cout << "Finished downloading " << protoDigest << " to \""
              << downloadDirectory << "\" in " << std::fixed
              << std::setprecision(3) << elapsed.count() << " second(s)"
              << std::endl;
    return 0;
}

int main(int argc, char *argv[])
{
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    try {
        using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
        using DataType = buildboxcommon::CommandLineTypes::DataType;
        using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
        using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;
        using CommandLine = buildboxcommon::CommandLine;

        const std::string additionalHelpText(
            "Description:\n"
            "  Download the specified digest(root/action/file) into "
            "the specified `--destination-dir` "
            "directory from the `--instance` of the CAS configured at "
            "`--cas-server`\n"
            "  Digest can be one of:\n"
            "    --root-digest   - Download a tree by its root node ID\n"
            "    --action-digest - Download the stderr, stdout, output "
            "files and directories of the specified action via the "
            "ActionCache. The ActionCache must be hosted in the "
            "same server as the CAS server at `--cas-server`\n"
            "    --file-digest   - Download a blob by its content to "
            "`--destination-dir`/blob\n\n"
            "Example usage:\n" +
            std::string(argv[0]) +
            " --instance=dev "
            "--cas-server=http://localhost:50051 "
            "--destination-dir=/path/to/output --action-digest=deadbeef/412");

        ArgumentSpec spec[] = {
            {"help", "Display usage and exit",
             TypeInfo(DataType::COMMANDLINE_DT_BOOL)},
            {"instance", "CAS server instance name",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG},
            {"cas-server", "CAS server end-point/url",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG},
            {"destination-dir", "Directory to save downloaded data",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG},
            {"root-digest", "Download a tree by its root node ID",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG},
            {"action-digest",
             "Download the stderr, stdout, output files and directories of "
             "this digest via the ActionCache",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG},
            {"file-digest", "Download a blob by its content",
             TypeInfo(DataType::COMMANDLINE_DT_STRING),
             ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG}};
        CommandLine commandLine(spec, additionalHelpText);
        const bool success = commandLine.parse(argc, argv);
        if (!success) {
            commandLine.usage();
            return 1;
        }

        if (commandLine.exists("help")) {
            commandLine.usage();
            return 0;
        }

        return runCasDownload(commandLine);
    }
    catch (const std::exception &e) {
        std::cerr << "ERROR: Caught exception [" << e.what()
                  << "] while running " << argv[0] << std::endl;
        return -1;
    }
}
