# logstreamtail

This issues LogStream API `ByteStream.Read()` requests and prints the data it reads to `stdout`.

## Usage

```
Usage: ./logstreamtail
   --help                 Display usage and exit [optional]
   --logstream-server     LogStream server address (for example: http://localhost:50070) [required]
   --resource-name        LogStream write resource name [required]
```
