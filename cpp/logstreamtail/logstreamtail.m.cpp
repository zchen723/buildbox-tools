/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logstreamtail.h>

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_logging.h>

#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <vector>

int main(int argc, char *argv[])
{
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(argv[0]);

    using buildboxcommon::CommandLineTypes;
    const std::vector<CommandLineTypes::ArgumentSpec> d_spec = {
        {"help", "Display usage and exit",
         CommandLineTypes::TypeInfo(
             CommandLineTypes::DataType::COMMANDLINE_DT_BOOL)},
        {"logstream-server",
         "LogStream server address (for example: http://localhost:50070)",
         CommandLineTypes::TypeInfo(
             CommandLineTypes::DataType::COMMANDLINE_DT_STRING),
         CommandLineTypes::ArgumentSpec::O_REQUIRED,
         CommandLineTypes::ArgumentSpec::C_WITH_ARG},
        {"resource-name", "LogStream write resource name",
         CommandLineTypes::TypeInfo(
             CommandLineTypes::DataType::COMMANDLINE_DT_STRING),
         CommandLineTypes::ArgumentSpec::O_REQUIRED,
         CommandLineTypes::ArgumentSpec::C_WITH_ARG},
    };

    buildboxcommon::CommandLine commandLine(d_spec);
    if (!commandLine.parse(argc, argv)) {
        commandLine.usage();
        return 1;
    }

    if (commandLine.exists("help")) {
        commandLine.usage();
        return 0;
    }

    try {
        buildboxcommon::ConnectionOptions connectionOptions;
        connectionOptions.setUrl(commandLine.getString("logstream-server"));

        const auto resourceName = commandLine.getString("resource-name");

        const LogStreamTail::DataAvailableCallback printFunction =
            [](const std::string &data) { std::cout << data << std::flush; };

        const auto status = LogStreamTail::readLogStream(
            connectionOptions, resourceName, printFunction);

        BUILDBOX_LOG_INFO("Read() returned: (" << status.error_code() << ", "
                                               << status.error_message()
                                               << ")");
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_ERROR("Caught exception ["
                           << e.what() << "] while running " << argv[0]);
        return -1;
    }
}
