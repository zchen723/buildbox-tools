cmake_minimum_required(VERSION 3.6)
project(logstreamreceiver CXX)
set(NAME logstreamreceiver)

if(${CMAKE_SYSTEM_NAME} MATCHES "AIX" AND ${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
    set(CMAKE_NO_SYSTEM_FROM_IMPORTED ON)
endif()

find_package(BuildboxCommon REQUIRED)

add_library(bytestreamservicer bytestreamservicer.cpp)
target_include_directories(bytestreamservicer PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(bytestreamservicer Buildbox::buildboxcommon)

add_executable(${NAME} ${NAME}.m.cpp)
target_include_directories(${NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(${NAME} bytestreamservicer)
install(TARGETS ${NAME} RUNTIME DESTINATION bin COMPONENT ${NAME})
