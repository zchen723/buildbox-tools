# outputstreamer

This program reads from `stdin` and streams it to the given ByteStream endpoint.

## Usage

```
Usage: ./outputstreamer
   --help                 Display usage and exit [optional]
   --logstream-server     LogStream server address (for example: http://localhost:50070) [required]
   --resource-name        LogStream write resource name [required]
```

## Example usage

```bash
cat /proc/cpuinfo | ./outputstreamer --logstream-server="http://localhost:50070" --resource-name="cpuinfo-output-log"
```

It can also be used interactively, hitting `Ctrl+D` (`^D`) to signal the end of file.

```bash
./outputstreamer --logstream-server="http://localhost:50070" --resource-name="user-input-log"
```

The `logstreamreceiver` tool in this repo can be used to receive those outputs on the other end.

