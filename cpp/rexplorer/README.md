# rexplorer

View information about an action and it's associated action-result in a JSON format, suitable for consumption by `jq`. It also supports a human-readable format directly by using the `--pretty` option.

## Usage
For basic usage with the CAS and Action Cache located at different endpoints, the following will print the Action and ActionResult (if available).
```
./rexplorer --cas-remote=http://foobar:50051 --cas-instance=foobar --ac-remote=http://ac:60051 --ac-instance=ac --action deadbeef/1337
```
If the CAS and Action Cache are located together, the `ac-*` options can be omitted and only the `cas-*` channel arguments specified.

## Output format
`protos/rexplorer.proto` contains the proto representation of the JSON output used by `rexplorer`. The main reason for using a custom format
is to inline parts of the Action which are normally referenced by digest, such as the `Command` or `InputRoot`. A basic action looks like the following (some empty fields left out/abbreviated for readability):

```
{
   "action": {
      "command": {
         "arguments": [
            "/bin/echo",
            "hi"
         ],
         "platform": {
            "properties": [
               {
                  "name": "ISA",
                  "value": "x86-64"
               }
            ]
         },
         "workingDirectory": "tmp",
         "outputPaths": [
            "my/output/file"
         ]
      },
      "inputRoot": [
         {
            "type": "directory",
            "name": "tmp"
         },
         {
            "type": "file",
            "name": "examplefile.txt"
         }
      ],
      "timeout": "0s"
   },
   "actionResult": {
      "outputFiles": [
         {
            "path": "my/output/file",
            "digest": {
               "hash": "deadbeefdeadbeef...",
               "sizeBytes": "9001"
            },
            "nodeProperties": {}
         }
      ],
      "exitCode": 0
      "stdoutDigest": {
         "hash": "abcdefabcdef...",
         "sizeBytes": "246810",
      },
      "executionMetadata": {
         "worker": "myworker1",
         ...
      }
   }
}
```

This can be easily filtered down using `jq`. For example to only print the `ActionResult` you can pipe the output of `rexplorer` to `jq` like so:

```
./rexplorer ... | jq .actionResult
```

### Output gotchas

There are some bits of weirdness with this output which might not be intuitive at first glance.

* The inputRoot is by default only fetched 1 level deep. More levels of the input root can be fetched using the `--depth` parameter.
* If an Action doesn't have an associated ActionResult, the `actionResult` key isn't returned at all, instead of having an "empty" value like `[]`
