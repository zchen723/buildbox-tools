/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <rexplorer_cmdlinespec.h>

#include <buildboxcommon_connectionoptions_commandline.h>

namespace rexplorer {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec()
{
    d_spec.emplace_back("help", "Display usage and exit",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));
    d_spec.emplace_back(
        "action", "Action digest to view. Must be of the form <hash>/<size>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_REQUIRED,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "depth",
        "How many levels of the input root to fetch, a negative "
        "value means print the entire root",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(1));

    d_spec.emplace_back("log-level", "Log verbosity level",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("info"));
    d_spec.emplace_back("verbose", "Set log level to debug",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL);

    d_spec.emplace_back("pretty", "Print human readable JSON",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));

    // CAS connection:
    const auto casConnectionOptionsSpec =
        buildboxcommon::ConnectionOptionsCommandLine("CAS", "cas-", true);
    d_spec.insert(d_spec.end(), casConnectionOptionsSpec.spec().cbegin(),
                  casConnectionOptionsSpec.spec().cend());

    // AC connection, defaults to CAS connection parameters if left out
    const auto acConnectionOptionsSpec =
        buildboxcommon::ConnectionOptionsCommandLine("Action Cache", "ac-",
                                                     false);
    d_spec.insert(d_spec.end(), acConnectionOptionsSpec.spec().cbegin(),
                  acConnectionOptionsSpec.spec().cend());
};

} // namespace rexplorer
