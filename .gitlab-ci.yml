stages:
  - build
  - test

.build_cpp_project:
    image: registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest
    stage: build

build_casdownload:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/casdownload && make -j$(nproc)
  artifacts:
    paths:
      - build/casdownload

build_casupload:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/casupload && make -j$(nproc) && ctest -V
  artifacts:
    paths:
      - build/casupload

build_outputstreamer:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/outputstreamer && make -j$(nproc)
  artifacts:
    paths:
      - build/outputstreamer

build_logstreamreceiver:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/logstreamreceiver && make -j$(nproc)
  artifacts:
    paths:
      - build/logstreamreceiver

build_logstreamtail:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/logstreamtail && make -j$(nproc) && ctest -V
  artifacts:
    paths:
      - build/logstreamtail

build_rexplorer:
  extends: .build_cpp_project
  script:
    - mkdir build && cd build && cmake ../cpp/rexplorer && make -j$(nproc)
  artifacts:
    paths:
      - build/rexplorer

test_logstreamutilities:
  image: registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest
  stage: test
  dependencies:
    - build_logstreamreceiver
    - build_outputstreamer
  script:

    # Stream stdin:
    - nohup build/logstreamreceiver --bind=localhost:50070 1>received_data 2>/dev/null &
    - sleep 3 # Give the server time to start
    - LOG_MESSAGE="Hello, world!"
    - echo -n "$LOG_MESSAGE" | build/outputstreamer --logstream-server=http://localhost:50070 --resource-name="test-stdin-log"
    - sleep 2
    - if [[ $(cat received_data) != "$LOG_MESSAGE" ]]; then exit 1; fi
    # Stream file:
    - nohup build/logstreamreceiver --bind=localhost:50071 1>received_data 2>/dev/null &
    - sleep 3 # Give the server time to start
    - dd if=/dev/urandom of=random_blob bs=1M count=16
    - build/outputstreamer --logstream-server=http://localhost:50071 --resource-name="test-file-log" --file=random_blob
    - sleep 2
    - diff received_data random_blob
    - exit $?

test_upload_and_download:
  image: registry.gitlab.com/buildgrid/buildbox/buildbox-casd:latest
  stage: test
  cache:
    key: upload-download-test-data
    paths:
      - test_data/
  dependencies:
    - build_casdownload
    - build_casupload
  before_script:
    - apt update && apt install -y curl
    - export CACHE_DIR=$(mktemp -d)
    - export CAS_SOCKET="unix:${CACHE_DIR}/casd.sock"
    - export DESTINATION_DIR=$(mktemp -d)
  script:
    # Launch casd:
    - nohup buildbox-casd "$CACHE_DIR" &
    # Populate data directory:
    - mkdir -p test_data/ && cd test_data/ && (curl "https://mirror.us-midwest-1.nexcess.net/gnu/hello/hello-2.1.0.tar.gz" | tar xz) && cd ..
    # Upload directory:
    - build/casupload --cas-server="$CAS_SOCKET" --output-digest-file="dir-digest.out" test_data/ || exit 1
    # And download it:
    - build/casdownload --cas-server="$CAS_SOCKET" --instance="" --root-digest="$(cat dir-digest.out)" --destination-dir="$DESTINATION_DIR" || exit 1
    - diff --brief --recursive test_data/ "$DESTINATION_DIR"


# Check C++ code formatting using clang-format
# Since the GitLab CI API doesn't currently provide an MR Commit SHA so that we can
# run all the files modified in the current MR (Single Commit is doable) we just
# run clang-format for the diff between "empty tree magic SHA" and the current commit
# on all the C++ files (by extension: c,cc,cpp,cxx,h)
# Discussion on the "empty tree magic SHA": https://stackoverflow.com/questions/9765453/
check_formatting:
    image: ubuntu:bionic
    stage: build
    before_script:
        - apt-get update && apt-get install -y clang-format-6.0 git-core
    script:
        - echo `which clang-format-6.0`
        - ln -s `which clang-format-6.0` /usr/bin/clang-format
        - export CLANG_FORMAT_SINCE_COMMIT="4b825dc642cb6eb9a060e54bf8d69288fbee4904"
        - linter_errors=$(git-clang-format-6.0 --commit "$CLANG_FORMAT_SINCE_COMMIT" -q --diff --style file --extensions c,cc,cpp,cxx,h  "$CI_PROJECT_DIR"| grep -v --color=never "no modified files to format" || true)
        - echo "$linter_errors"
        - if [[ ! -z "$linter_errors" ]]; then echo "Detected formatting issues; please fix"; exit 1; else echo "Formatting is correct"; exit 0; fi

e2e:
    image:
      name: docker/compose:1.23.2
      entrypoint: ["/bin/sh", "-c"]
    stage: test
    variables:
        DOCKER_HOST: tcp://docker:2375
    services:
      - docker:dind
    before_script:
      - docker-compose -v
    script:
      - docker-compose -f cpp/casdownload/docker-compose.yaml build
      - docker-compose -f cpp/casdownload/docker-compose.yaml up --exit-code-from casdownload
